const IsAuthenticated = () => {

    const statusAuth = window.localStorage.getItem("access_token") || window.localStorage.getItem("refresh_token");
    const authenticated = statusAuth ? true : false

    return(
      { authenticated }
    )
  }

export default IsAuthenticated;
