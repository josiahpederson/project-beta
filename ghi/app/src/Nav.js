import { NavLink } from 'react-router-dom';
import Dropdown from 'react-bootstrap/Dropdown';
import IsAuthenticated from './authenticate';
import React from 'react';
import { Nav } from 'react-bootstrap';
import mainLogo from './Images/carcar-super-final-logo.png';


// class Nav extends React.Component {
//   constructor() {
//     super();
//     this.state = {
//       authenticated: false,
//     }
//   }
//   render () {

function Navl() {
  const status = IsAuthenticated()
    return (
      <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
        <div className="container-fluid">
          <NavLink className="navbar-brand" to="/">
            <img className="logo" src={mainLogo} alt="CarCar"/>
          </NavLink>
          <button
            className="navbar-toggler" type="button" data-bs-toggle="collapse"
            data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
            aria-expanded="false" aria-label="Toggle navigation">
            <span className="navbar-toggler-icon"></span>
          </button>
          <div className="collapse navbar-collapse" id="navbarSupportedContent">
            <ul className="navbar-nav me-auto mb-2 mb-lg-0">
                    {
                    status.authenticated ?
              <li classname="nav-item">
                <Dropdown>
                  <Dropdown.Toggle variant="dark" id="sales-dropdown">
                    Sales
                  </Dropdown.Toggle>
                  <Dropdown.Menu>
                    <Dropdown.Item href="/autosales">List Autosales</Dropdown.Item>
                    <Dropdown.Item href="/salespeople">List Salespeople</Dropdown.Item>
                        <Dropdown.Item href="/salespeople/new">Add Salesperson</Dropdown.Item>
                        <Dropdown.Item href="/autosales/new">Add Autosale</Dropdown.Item>
                        <Dropdown.Item href="/customers/new">Add Customer</Dropdown.Item>

                  </Dropdown.Menu>
                </Dropdown>
              </li>
                    :
                      null
                    }

              <li className="nav-item>">
                <Dropdown>
                  <Dropdown.Toggle variant="dark" id="inventory-dropdown">
                    Inventory
                  </Dropdown.Toggle>
                  <Dropdown.Menu>
                    <Dropdown.Item href="/manufacturers">List Manufacturers</Dropdown.Item>
                    <Dropdown.Item href="/models">List Models</Dropdown.Item>
                    <Dropdown.Item href="/automobiles">List Automobiles</Dropdown.Item>
                    {
                    status.authenticated ?
                      <React.Fragment>
                    <Dropdown.Item href="/manufacturers/new">Add Manufacturer</Dropdown.Item>
                    <Dropdown.Item href="/models/new">Add Model</Dropdown.Item>
                    <Dropdown.Item href="/automobiles/new">Add Automobile</Dropdown.Item>
                    </React.Fragment>
                    :
                      null
                    }
                  </Dropdown.Menu>
                </Dropdown>
              </li>

              <li className="nav-item">
                <Dropdown>
                  <Dropdown.Toggle variant="dark" id="service-dropdown">
                    Service
                  </Dropdown.Toggle>
                  <Dropdown.Menu>
                    <Dropdown.Item href="/service/new">Book Appointment</Dropdown.Item>
                    {
                    status.authenticated ?
                      <React.Fragment>
                    <Dropdown.Item href="/technicians/new">Add Technicians</Dropdown.Item>
                    <Dropdown.Item href="/service">Upcoming Appointments</Dropdown.Item>
                    <Dropdown.Item href="/service/history">Service History</Dropdown.Item>
                    </React.Fragment>
                    :
                      null
                    }
                  </Dropdown.Menu>
                </Dropdown>
              </li>
              {
                status.authenticated ?
                  <React.Fragment>
                    <Nav className='ms-auto'>
                      <Nav.Link className="ml-auto" href='/logout' to="/logout">Logout</Nav.Link>
                    </Nav>
                  </React.Fragment>
                  :
                  <React.Fragment>
                    <Nav className='ms-auto'>
                      <Nav.Link className="ml-auto" href='/login' to="/login">Login</Nav.Link>
                    </Nav>
                  </React.Fragment>
                  }
            </ul>
          </div>
        </div>
      </nav>
    )
  }



export default Navl;
