from django.test import SimpleTestCase
from ..models import ServiceAppointment
from datetime import datetime

# Create your tests here.

class ServiceAppointmentTestCase(SimpleTestCase):
    def test_str_happy_path(self):
        # setup
        appointment = ServiceAppointment(
            owner = "Johnny",
            vin = "29FKGJCNDJFUR8576",
            scheduled_time = datetime.now(),
            reason = "crash",
        )

        # assertion
        self.assertEqual(
            str(appointment),
            "Johnny's vin # 29FKGJCNDJFUR8576 checkup for crash"    
        )

        # cleanup --not required for unittests

    # def test_str_no_owner(self):
    #     # setup
    #     appointment = ServiceAppointment(
    #         owner = "",
    #         vin = "29FKGJCNDJFUR8576",
    #         scheduled_time = datetime.now(),
    #         reason = "crash",
    #     )

    #     # assertion
    #     self.assertEqual(
    #         str(appointment),
    #         "vin # 29FKGJCNDJFUR8576 checkup for crash"    
    #     )

        # cleanup --not required for unittests
