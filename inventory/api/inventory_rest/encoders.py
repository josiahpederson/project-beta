from ast import Mod
from common.json import ModelEncoder

from .models import Automobile, Manufacturer, VehicleModel, AddOn


class ManufacturerEncoder(ModelEncoder):
    model = Manufacturer
    properties = [
        "id",
        "name",
    ]


class VehicleModelEncoder(ModelEncoder):
    model = VehicleModel
    properties = [
        "id",
        "name",
        "picture_url",
        "base_price",
        "manufacturer",
    ]
    encoders = {
        "manufacturer": ManufacturerEncoder(),
    }


class VehicleModelBasicEncoder(ModelEncoder):
    model=VehicleModel
    properties = [
        "id",
        "name",
    ]


class AddOnModelEncoder(ModelEncoder):
    model = AddOn
    properties = [
        "name",
        "price",
        "picture_url",
        "id",
        "model",
    ]

    encoders = {
        "model": VehicleModelBasicEncoder()
    }



class AutomobileEncoder(ModelEncoder):
    model = Automobile
    properties = [
        "id",
        "color",
        "year",
        "vin",
        "model",
        "sold",
 
    ]
    encoders = {
        "model": VehicleModelEncoder(),
    }
